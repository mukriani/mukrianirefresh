module.exports = exports = (server, pool) => {
    server.post('/api/refreshpost', (req, res) => {
        console.log(req.body);
        const {
            email,
            bersedia,
            tolak,
            nama,
            umur,
            no_hp,
            dosen,
            tenaga_kependidikan,
            cleaning_service,
            security,
            mahasiswa,
            other_status,
            status_other,
            kelurahan,
            kecamatan,
            kabupaten,
            pertanian,
            biologi,
            ekonomi,
            peternakan,
            hukum,
            ilmu_sosial,
            kedokteran,
            teknik,
            ilmu_kesehatan,
            mipa,
            program_pascasarjana,
            ilmu_budaya,
            perikanan,
            other_fakultas,
            fakultas_other,
            jurusan,
            demam,
            batuk,
            sesak,
            tidak_keluhan,
            jakarta,
            bandung,
            yogyakarta,
            depok,
            tangerang,
            bogor,
            manado,
            pontianak,
            solo,
            denpasar,
            tidak_riwayat,
            other_riwayat,
            riwayat_other,
            kontak_pasien,
            bekerja_kesehatan,
            memiliki_demam,
            tidak_resiko,
            kendaraan_pribadi,
            kereta_api,
            bus,
            angkot,
            taksi,
            ojek_online,
            other_kendaraan,
            kendaraan_other
        } = req.body;
        pool.query(`INSERT INTO public."refresh_project"("email", "bersedia", "tolak", "nama", "umur", "no_hp", "dosen", "tenaga_kependidikan", "cleaning_service", "security", "mahasiswa", "other_status", "status_other", "kelurahan", 
        "kecamatan", "kabupaten", "pertanian", "biologi", "ekonomi", "peternakan", "hukum", "ilmu_sosial", "kedokteran", "teknik", "ilmu_kesehatan",
        "mipa", "program_pascasarjana", "ilmu_budaya", "perikanan", "other_fakultas", "fakultas_other", "jurusan", "demam", "batuk", "sesak", "tidak_keluhan",
        "jakarta", "bandung", "yogyakarta", "depok", "tangerang", "bogor", "manado", "pontianak", "solo", "denpasar", "tidak_riwayat", "other_riwayat", "riwayat_other",
        "kontak_pasien", "bekerja_kesehatan", "memiliki_demam", "tidak_resiko", "kendaraan_pribadi","kereta_api", "bus", "angkot", "taksi", "ojek_online",
        "other_kendaraan", "kendaraan_other")			
            VALUES ('${email}', '${bersedia}', '${tolak}', '${nama}', '${umur}', '${no_hp}', '${dosen}', '${tenaga_kependidikan}', '${cleaning_service}', '${security}', '${mahasiswa}', '${other_status}', '${status_other}', '${kelurahan}', 
                '${kecamatan}', '${kabupaten}', '${pertanian}', '${biologi}', '${ekonomi}', '${peternakan}', '${hukum}', '${ilmu_sosial}', '${kedokteran}', '${teknik}', '${ilmu_kesehatan}',
                '${mipa}', '${program_pascasarjana}', '${ilmu_budaya}', '${perikanan}', '${other_fakultas}', '${fakultas_other}', '${jurusan}', '${demam}', '${batuk}', '${sesak}', '${tidak_keluhan}',
                '${jakarta}', '${bandung}', '${yogyakarta}', '${depok}', '${tangerang}', '${bogor}', '${manado}', '${pontianak}', '${solo}', '${denpasar}', '${tidak_riwayat}', '${other_riwayat}', '${riwayat_other}',
                '${kontak_pasien}', '${bekerja_kesehatan}', '${memiliki_demam}', '${tidak_resiko}', '${kendaraan_pribadi}','${kereta_api}', '${bus}', '${angkot}', '${taksi}', '${ojek_online}',
                '${other_kendaraan}', '${kendaraan_other}');`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data ${nama} berhasil disimpan`
                })
            }
        })

    });

    server.get('/api/getrefresh', (req, res) => {
        var query = `SELECT "nama", "no_hp", "email", "jurusan" FROM "refresh_project"`
        pool.query(query,

            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    })
                } else {
                    res.send(200, {
                        success: true,
                        result: result.rows
                    })
                }
            }
        );
        console.log(query)
    });


    server.put('/api/updaterefresh/:id', (req, res) => {
        const id = req.params.id;
        const {
            email,
            bersedia,
            tolak,
            nama,
            umur,
            no_hp,
            dosen,
            tenaga_kependidikan,
            cleaning_service,
            security,
            mahasiswa,
            other_status,
            status_other,
            kelurahan,
            kecamatan,
            kabupaten,
            pertanian,
            biologi,
            ekonomi,
            peternakan,
            hukum,
            ilmu_sosial,
            kedokteran,
            teknik,
            ilmu_kesehatan,
            mipa,
            program_pascasarjana,
            ilmu_budaya,
            perikanan,
            other_fakultas,
            fakultas_other,
            jurusan,
            demam,
            batuk,
            sesak,
            tidak_keluhan,
            jakarta,
            bandung,
            yogyakarta,
            depok,
            tangerang,
            bogor,
            manado,
            pontianak,
            solo,
            denpasar,
            tidak_riwayat,
            other_riwayat,
            riwayat_other,
            kontak_pasien,
            bekerja_kesehatan,
            memiliki_demam,
            tidak_resiko,
            kendaraan_pribadi,
            kereta_api,
            bus,
            angkot,
            taksi,
            ojek_online,
            other_kendaraan,
            kendaraan_other
        } = req.body;

        pool.query(`UPDATE public."refresh_project"
        SET 
        "email"='${email}', 
        "bersedia"='${bersedia}', 
        "tolak"='${tolak}', 
        "nama"='${nama}', 
        "umur"='${umur}', 
        "no_hp"='${no_hp}', 
        "dosen"='${dosen}', 
        "tenaga_kependidikan"='${tenaga_kependidikan}', 
        "cleaning_service"='${cleaning_service}', 
        "security"='${security}', 
        "mahasiswa"='${mahasiswa}', 
        "other_mahasiswa"='${other_status}', 
        "status"='${status_other}', 
        "kelurahan"='${kelurahan}', 
        "kecamatan"='${kecamatan}', 
        "kabupaten"='${kabupaten}', 
        "pertanian"='${pertanian}', 
        "biologi"='${biologi}', 
        "ekonomi"='${ekonomi}', 
        "peternakan"='${peternakan}', 
        "hukum"='${hukum}', 
        "ilmu_sosial"='${ilmu_sosial}', 
        "kedokteran"='${kedokteran}', 
        "teknik"='${teknik}', 
        "ilmu_kesehatan"='${ilmu_kesehatan}',
        "mipa"='${mipa}', 
        "program_pascasarjana"='${program_pascasarjana}', 
        "ilmu_budaya"='${ilmu_budaya}', 
        "perikanan"='${perikanan}', 
        "other_fakultas"='${other_fakultas}', 
        "fakultas_other"='${fakultas_other}', 
        "jurusan"='${jurusan}', 
        "demam"='${demam}', 
        "batuk"='${batuk}', 
        "sesak"='${sesak}, 
        "tidak_keluhan"='${tidak_keluhan}',
        "jakarta"='${jakarta}', 
        "bandung"='${bandung}', 
        "yogyakarta"='${yogyakarta}', 
        "depok"='${depok}', 
        "tangerang"='${tangerang}', 
        "bogor"='${bogor}', 
        "manado"='${manado}', 
        "pontianak"='${pontianak}', 
        "solo"='${solo}', 
        "denpasar"='${denpasar}', 
        "tidak_riwayat"='${tidak_riwayat}', 
        "other_riwayat"='${other_riwayat}', 
        "riwayat_other"='${riwayat_other}',
        "kontak_pasien"='${kontak_pasien}', 
        "bekerja_kesehatan"='${bekerja_kesehatan}', 
        "memiliki_demam"='${memiliki_demam}', 
        "tidak_resiko"='${tidak_resiko}', 
        "kendaraan_pribadi"='${kendaraan_pribadi}',
        "kereta_api"='${kereta_api}', 
        "bus"='${bus}', 
        "angkot"='${angkot}', 
        "taksi"='${taksi}', 
        "ojek_online"='${ojek_online}',
        "other_kendaraan"='${other_kendaraan}', 
        "kendaraan_other"='${kendaraan_other}'
        WHERE "id"= ${id};`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                });

            } else {
                res.send(200, {
                    success: true,
                    result: `Data ${nama} Berhasil diubah`
                });

            }

        });

    });

    server.get('/api/getrefreshbyid/:id', (req, res) => {
        const id = req.params.id;
        pool.query(`SELECT "email", "bersedia", "tolak", "nama", "umur", "no_hp", "dosen", "tenaga_kependidikan", "cleaning_service", "security", "mahasiswa", "other_status", "status_other", "kelurahan", 
        "kecamatan", "kabupaten", "pertanian", "biologi", "ekonomi", "peternakan", "hukum", "ilmu_sosial", "kedokteran", "teknik", "ilmu_kesehatan",
        "mipa", "program_pascasarjana", "ilmu_budaya", "perikanan", "other_fakultas", "fakultas_other", "jurusan", "demam", "batuk", "sesak", "tidak_keluhan",
        "jakarta", "bandung", "yogyakarta", "depok", "tangerang", "bogor", "manado", "pontianak", "solo", "denpasar", "tidak_riwayat", "other_riwayat", "riwayat_other",
        "kontak_pasien", "bekerja_kesehatan", "memiliki_demam", "tidak_resiko", "kendaraan_pribadi","kereta_api", "bus", "angkot", "taksi", "ojek_online",
        "other_kendaraan", "kendaraan_other"
        FROM public."refresh_project" where "id"=${id}`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: result.rows[0]
                })
            }
        })

    });


    server.put('/api/deleterefresh/:id', (req, res) => {
        const id = req.params.id;
        const { nama } = req.body;

        pool.query(`update public."refresh_project" 
        WHERE "id"='${id}'`,
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        result: error
                    });
                } else {
                    res.send(200, {
                        success: true,
                        result: `Data ${nama} berhasil dihapus`
                    });
                }
            }
        )
    });

}