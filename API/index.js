const Restify = require("restify");
const corsMidWare = require('restify-cors-middleware')
const server = Restify.createServer({
    name: "Server Web API",
    version: "1.0.0"
})

server.use(Restify.plugins.bodyParser({
        mapParams: false
    }))
    //configuration untuk connect ke database
global.config = require('./config/connection');


//config untuk cors ke react
const cors = corsMidWare({
    origins: ['http://localhost:3000'],
    allowHeaders: ['x-access-token'],
    exposeHeader: []
})
server.pre(cors.preflight);
server.use(cors.actual);
//end config cors

//Example API
server.get('/api/Barang/:id', (req, res) => {
    const id = req.params.id;
    res.send(200, {
        data: {
            id: id,
            Nama: "mukri",
            Alamat: 'Radio Dalam',
            umur: '23',
            No: '085668129148',
            Gender: 'Famale',
            Katagori: "X31"

        }
    })
});

//import service
require('./service/RefreshService')(server, global.config.pool);


server.listen(4000, function() {
    console.log('server ', server.name, " SUccess Listen . . .");

});