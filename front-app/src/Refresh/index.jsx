import React from 'react';
import FormInput from './forminput';
import Table from 'react-bootstrap/Table';
import RefreshService from '../service/RefreshService';
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import {ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, CardTitle, CardText, Collapse } from 'reactstrap'
import Dropdown from 'react-bootstrap/Dropdown'

class Refresh extends React.Component {
    RefreshModel = {
        id: 0,
        email:"",
            bersedia:false,
            tolak:false,
            nama:"",
            umur:0,
            no_hp:0,
            dosen:false,
            tenaga_kependidikan:false,
            cleaning_service:false,
            security:false,
            mahasisw:false,
            other_status:false,
            status_other:"",
            kelurahan:"",
            kecamatan:"",
            kabupaten:"",
            pertanian:false,
            biologi:false,
            ekonomi:false,
            peternakan:false,
            hukum:false,
            ilmu_sosial:false,
            kedokteran:false,
            teknik:false,
            ilmu_kesehatan:false,
            mipa:false,
            program_pascasarjana:false,
            ilmu_budaya:false,
            perikanan:false,
            other_fakultas:false,
            fakultas_other:"",
            jurusan:"",
            demam:false,
            batuk:false,
            sesak:false,
            tidak_keluhan:false,
            jakarta:false,
            bandung:false,
            yogyakarta:false,
            depok:false,
            tangerang:false,
            bogor:false,
            manado:false,
            pontianak:false,
            solo:false,
            denpasar:false,
            tidak_riwayat:false,
            other_riwayat:false,
            riwayat_other:"",
            kontak_pasien:false,
            bekerja_kesehatan:false,
            memiliki_demam:false,
            tidak_resiko:false,
            kendaraan_pribadi:false,
            kereta_api:false,
            bus:false,
            angkot:false,
            taksi:false,
            ojek_online:false,
            other_kendaraan:false,
            kendaraan_other:""
    }

    constructor() {
        super();
        this.state = {
            ListRefresh: [],
            RefreshModel: this.RefreshModel,
            openmodalEdit: false,
            openmodalDetail: false,
            openmodalDelete: false,
            hidden: true,
            errors: {},
        }
    }

    handleValidation() {
        let fields = this.state.RefreshModel;
        let errors = {};
        let formIsValid = true;

        //Client
        if (!fields["nama"]) {
            formIsValid = false;
            errors["nama"] = "Pertanyaan ini wajib diisi";
        }

        //Nama Projek
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Pertanyaan ini wajib diisi";
        }

        //Star
        if (!fields["umur"]) {
            formIsValid = false;
            errors["umur"] = "Pertanyaan ini wajib diisi";
        }

        //Role
        if (!fields["no_hp"]) {
            formIsValid = false;
            errors["no_hp"] = "Pertanyaan ini wajib diisi";
        }

        //Phase
        if (!fields["kelurahan"]) {
            formIsValid = false;
            errors["kelurahan"] = "Pertanyaan ini wajib diisi";
        }

        //Star
        if (!fields["kecamatan"]) {
            formIsValid = false;
            errors["kecamatan"] =  "Pertanyaan ini wajib diisi";
        }
        //Star
        if (!fields["kabupaten"]) {
            formIsValid = false;
            errors["kabupaten"] =  "Pertanyaan ini wajib diisi";
        }
        //Tanggal
        if (fields["jurusan"]){
            formIsValid = false;
            errors["jurusan"] =  "Pertanyaan ini wajib diisi";
        }

        this.setState({ errors: errors });
        return formIsValid;
    }


    componentDidMount() {
        this.loadList();

    }


    loadList = async () => {
        const respon = await RefreshService.getAllrefresh();
        if (respon.success) {
            this.setState({
                ListRefresh: respon.result,
            })
        }
    }


    handleOpen = () => {
        this.setState({
            hidden: true,
            openmodalEdit: true,
            RefreshModel: this.RefreshModel,
            mode: 'create'
        })
    }


    cancelHandle = async () => {
        this.setState({
            openmodalEdit: false,
            errors: {}

        });

    }

    
    checkedHandler = val => ({ target: { checked } }) => {
        this.setState({
            RefreshModel: {
                ...this.state.RefreshModel,
                [val]: checked
            }
        })
    }
    handleEdit = async (id) => {
        const respon = await RefreshService.getdatabyid(id);
        if (respon.success) {
            this.setState({
                hidden: false,
                openmodalEdit: true,
                RefreshModel: respon.result,
                mode: "edit"

            })
        }
        else {
            alert('error' + respon.result);
        }
    }


    cancelHandleEdit = async () => {
        // const respon = await BarangService.getAll();
        // if (respon.success) {
        this.setState({
            // BarangModel: respon.result,
            openmodalDelete: false,
            errors: {}

        });

    }

    cancelHandleDelete = async () => {
        this.setState({
            openmodalDelete: false

        });

    }

  

    onSave = async () => {
        const { RefreshModel, mode } = this.state;
        if (this.handleValidation()) {
            if (mode === 'create') {
                const respons = await RefreshService.post(RefreshModel);
                if (respons.success) {
                    alert('Success : ' + respons.result)
                    this.loadList();
                }
                else {
                    alert('Error : ' + respons.result)
                }
                this.setState({
                    hidden: true,
                    openmodalEdit: false
                });
            } else {

                const respon = await RefreshService.updaterefresh(RefreshModel);
                if (respon.success) {
                    alert('Success : ' + respon.result)
                    this.loadList();
                }
                else {
                    alert('Error : ' + respon.result)
                }
                this.setState({
                    hidden: true,
                    openmodalEdit: false
                });
            }
        } else {
            alert("Terjadi kesalahan dalam pengisian form")
        }
    }



    handleDelete = async (id) => {
        const respon = await RefreshService.getdatabyid(id);
        if (respon.success) {
            this.setState({
                hidden:false,
                openmodalDelete: true,
                RefreshModel: respon.result

            });
        }
        else {
            alert('error' + respon.result);
        }
    }

    sureDelete = async (item) => {
        const { RefreshModel} = this.state;
        const respons = await RefreshService.delete(RefreshModel);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList();
        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodalDelete: false
        });
    }

    changeHandler = name => ({ target: { value } }) => {
        this.setState({
            RefreshModel: {
                ...this.state.RefreshModel,
                [name]: value,
                

            }
        })
    }

    

    sureDelete = async (item) => {
        const { RefreshModel } = this.state;
        const respons = await RefreshService.delete(RefreshModel);
        if (respons.success) {
            alert('Success : ' + respons.result)
            this.loadList();
        }
        else {
            alert('Error : ' + respons.result)
        }
        this.setState({
            openmodalDelete: false
        });
    }

    handleDetail = async (id) => {
        const respon = await RefreshService.getdetabyid(id);
        console.log(respon)
        if (respon.success) {
            this.setState({
                hidden:false,
                RefreshModel:respon.result,
                openmodalDetail: true,


            })
        }
    else {
        alert('error' + respon.result);
    }
}

    handleCancelDetail = async () => {
        this.setState({
            openmodalDetail: false
        })
    }


    render() {
        const { ListRefresh, RefreshModel, hidden, openmodalEdit,errors, mode, openmodalDelete, handleCancelDetail,
            handleDetail, openmodalDetail } = this.state;
        return (

            <div>
               
                <FormInput
                    ListRefresh={ListRefresh}
                    errors={errors}
                    openmodalDetail={openmodalDetail}
                    handleDetail={this.handleDetail}
                    RefreshModel={RefreshModel}
                    hidden={hidden}
                    title={mode}
                    handleCancelDetail={this.handleCancelDetail}
                    openmodalEdit={openmodalEdit}
                    handleOpen={this.handleOpen}
                    openmodalDelete={openmodalDelete}
                    mode={mode}
                    changeHandler={this.changeHandler}
                    cancelHandle={this.cancelHandle}
                    onSave={this.onSave}
                    checkedHandler={this.checkedHandler}
                    sureDelete={this.sureDelete}
                />
                <span class="glyphicon glyphicon-plus-sign" onClick={() => this.handleOpen()}></span>
                 <CardText style={{color:'#000066', fontStyle:'Roboto' }}> 
                 <h4>REFRESH PROJECT </h4>
                <Row style={{borderTopStyle:'double',borderTopColor:'#000066',width:'90%',Align:'Center',position:'relative',left:'40px',right:'40px'}}>
                <table class='table table-striped table-xl table-hover' style={{borderTopStyle:'ridge',borderTopColor:'#000066',padding:'8px',borderTopWidth:'2px',textAlign:'-moz-initial',width:'100%'}}>
                    <thead>
                        <tr style={{borderBottomStyle:'ridge',border:'1',borderBottomColor:'#000066',borderBottomWidth:'2px',textAlign:'-moz-ridge',width:'90%'}}>
                            <th>Email</th>
                            <th>Nama</th>
                            <th>No. Hp</th>
                            <th>Jurusan</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            ListRefresh.map(ref => {
                                return (
                                    <tr style={{borderTopStyle:'ridge',bgColor:'red',fontColor:'#000066',border:'1',borderTopColor:'#000066' ,borderTopWidth:'2px',textAlign:'-moz-ridge',width:'90%'}} >
                                        <td>{ref.email}</td>
                                        <td>{ref.nama}</td>
                                        <td>{ref.no_hp}</td>
                                        <td>{ref.jurusan}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button style={{ backgroundColor: "#000066" }} type="button" class="btn btn-info"><font color ="white">More</font></button>
                                                <button style={{ backgroundColor: "#000066" }} type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                   <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#" onClick={() => this.handleDetail(ref.id)}>Detail</a></li>
                                                    <li><a href="#" onClick={() => this.handleEdit(ref.id)}>Ubah</a></li>
                                                    <li><a href="#" onClick={() => this.handleDelete(ref.id)}>Hapus</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                </Row>
             
                {/* <Modal show={openmodalDelete} style={{ opacity: 1 }}>
                <Modal.Header style={{ backgroundColor: "red" }}>
                <Modal.Title style={{ backgroundColor: "red" }}><font color="white"> &nbsp;Hapus ?</font></Modal.Title></Modal.Header>
                    <Modal.Body> <div class="col-sm-2"><br/><h6><span style={{ color: "red",fontSize:'60px' }} class="glyphicon glyphicon-trash "></span></h6></div> &nbsp; 
                    <div class="col-sm-10"> <h4><font color ="red">Anda yakin ingin menghapus project di <br/> <b>{RefreshModel.name}</b> ?</font></h4></div></Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                        <button style={{backgroundColor:'#ff6600', borderColor:'#ff6600', color:'white'}} onClick={() => this.cancelHandleDelete()}>Tidak</button>
                            <button style={{backgroundColor:"red", borderColor:"red", color:'white'}} onClick={() => this.sureDelete()}>&nbsp;Ya&nbsp;</button>
                        </div>
                        </Modal.Footer>
                </Modal>
                
                <Modal show={openmodalDetail} style={{ opacity: 1 }} size='lg'>
                    <Modal.Header style={{ backgroundColor: "#000066" }}>
                        <Modal.Title><h4><font color="white">Detail</font></h4></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                

<body>
<div class="container">
  <div id="accordion"> 
                       
                        <div class="card">
    <div class="card-header">
      <a style={{borderColor:'#778899', fontColor:'black', fontSize:'12px'}} class="collapsed card-link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
      <b>DATA</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                               
                               <span class="fa fa-caret-down"></span>
      </a>
    </div>
    <div id="collapseOne" class="collapse">
      <div class="card-body">
      <div class="col-sm-6">
                                        <label style={{color:'navy',fontSize:'11px'}} className='form-label'> Email</label><br />
                                        <label><h5><b><font color='navy'>{RefreshModel.email}</font></b></h5> </label><br />
                                        <label style={{color:'navy',fontSize:'11px'}} className='form-label'> Nama Lengkap</label><br />
                                        <label><h5><b><font color='navy'>{RefreshModel.nama}</font></b></h5></label><br />
                                        <label style={{color:'navy',fontSize:'11px'}} className='form-label'> Umur</label><br />
                                        <label><h5><b><font color='navy'>{RefreshModel.umur}</font></b></h5> </label><br />
                                        <label style={{color:'navy',fontSize:'11px'}} className='form-label'> Nomer Handphone</label><br />
                                        <label><h5><b><font color='navy'>{RefreshModel.no_hp}</font></b></h5> </label><br />
                                        </div>
                                        <div class="col-sm-6">
                                        <label style={{color:'navy',fontSize:'11px'}} className='form-label'>Kelurahan</label><br />
                                        <label><h5><b><font color='navy'>{RefreshModel.kelurahan}</font></b></h5> </label><br />
                                        <label style={{color:'navy',fontSize:'11px'}} className='form-label'>Kecamatan</label><br />
                                        <label><h5><b><font color='navy'>{RefreshModel.kecamatan}</font></b></h5></label><br />
                                        <div class="form-row">
                                        <div class="col-sm-6">
                                        <label style={{color:'navy',fontSize:'11px'}} className='form-label'>Kabupaten</label><br />
                                        <label><h5><b><font color='navy'>{RefreshModel.kabupaten}</font></b></h5> </label><br />
                                        </div>
                                        <div class="col-sm-6">
                                        <label style={{color:'navy',fontSize:'11px'}} className='form-label'>Jurusan</label><br />
                                        <label><h5><b><font color='navy'>{RefreshModel.jurusan}</font></b></h5> </label><br />
                                        </div>
                                        </div>
                                    </div>
      </div>
    </div>
  </div>
 
  <div class="card">
    <div class="card-header">
      <a style={{ borderColor:'#778899', fontColor:'black', fontSize:'12px'}} class="collapsed card-link" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
      <b>STATUS</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                               
                               <span class="fa fa-caret-down"></span>
      </a>
    </div>
    <div id="collapseTwo" class="collapse">
      <div class="card-body">
      <label><h5><b><font color='navy'>{RefreshModel.pertanian}</font></b></h5> </label><br />
      </div>
    </div>
  </div>
 
  <div class="card">
    <div class="card-header">
      <a style={{borderColor:'#778899', fontColor:'black', fontSize:'12px'}} class="collapsed card-link" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
      <b>RIWAYAT</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                               
                                <span class="fa fa-caret-down"></span>
      </a>
    </div>
    <div id="collapseThree" class="collapse">
      <div class="card-body">
      <label><h5><b><font color='navy'>{RefreshModel.demam}</font></b></h5> </label><br />
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <a style={{borderColor:'#778899', fontColor:'black', fontSize:'12px'}} class="collapsed card-link" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
      <b>TRANSPORTASI</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="fa fa-caret-down"></span>
      </a>
    </div>
    <div id="collapseFour" class="collapse">
      <div class="card-body">
      <label><h5><b><font color='navy'>{RefreshModel.kendaraan_pribadi}</font></b></h5> </label><br />
      </div>
    </div>
  </div>
  </div>
</div>
</body>

                    </Modal.Body>
                    <Modal.Footer>
                        <div className="modal-footer">
                            <button type="button" style={{backgroundColor:'orangeRed',color:'white'}} className="btn btn-warning" onClick={() => this.handleCancelDetail()}> Close </button>
                        </div>
                    </Modal.Footer>
                </Modal> */}

                </CardText>

            </div>
        )
    }
}

export default Refresh;


