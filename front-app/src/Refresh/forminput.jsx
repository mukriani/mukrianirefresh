import React from 'react';
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Select from 'react-select';
import Modal from 'react-bootstrap/Modal';

class FormInputRefresh extends React.Component {
    constructor() {
        super();
        this.state = {
        }
    }
    render() {
        const {RefreshModel, mode, checkedHandler, changeHandler, cancelHandle, onSave, openmodalEdit, handleCancel, errors} = this.props;       
      
        return (
            <div>
                <Modal show={openmodalEdit} style={{opacity:3}}>
                    <Modal.Header style={{ backgroundColor: "#000066", width:'150%' }}>
                        <Modal.Title style={{backgroundColor: "#000066",width:'150%' }}><h4><font color="white">
                            Surveilans Coronavirus Disease 2019 (COVID 2019) Sivitas Akademika Universitas Singaperbangsa Karawang tahap II
                            </font></h4> <h6><font color="white">Sebagai bagian dari upaya kesiapsiagaan dalam menghadapi meluasnya penularan COVID 19, maka perlu dilakukan serveilans terutama pada sivitas akademika Unsoed. 
                                Form Surveilans ini diinisiasi oleh Tin Tanggap Pademi COVID-19 Unsoed, data yang dikumpulkan pada form ini akan dijaga
                                kerahasiaannya dan digunakan hanya UNTUK KEPENTINGAN SURVEILANS. Untuk informasi lebih lanjut anda dapat menghubungi nomer pusat layanan
                                COVID-19 Universitas Jenderal Soedirman melalui kontak telepon (0281) 641233 atau WA 081 229 366 919 
                                &emsp; Wajib* </font></h6></Modal.Title>
                        {/* {JSON.stringify(ResourceModel)} */}
                    </Modal.Header>
                    <Modal.Body style={{backgroundColor: "white",width:'150%' }}>
                        <div className='form-inside-input'>
                            <label className='form-label'> Alamat Email*</label><br/>
                            <input type="text" id="email" className="form-control"
                            value={RefreshModel.email} onChange={changeHandler("email")}/><br/>
                            {/* <span style={{color: "red"}}>{errors["email"]}</span><br/> */}
                            Form ini diisi berdasarkan kesukarelaan dan tidak terdapat paksaan. Dengan berpatisipasi mengisi form ini,
                            anda menyatakan bersedia bahwa info kontak yang disediakan dapat dihubungi lebih lanjut oleh Tim Tanggap PademiCovid-19 
                            Unsoed apabila dianggap perlu. Semua informasi bersifat rahasia, dikumpulkan serta dianalisis untk kepentingan 
                            kesiapsiagaan Unsoed menghadapi perkembangan infeksi Covid-19. Berikut ini saya menyatakan:*
                            <input type="checkbox" id="bersedia" className="form-check" 
                            checked={RefreshModel.bersedia} onChange={checkedHandler("bersedia")} /><label className="form-check-label" htmlFor="bersedia">Bersedia</label>
                              <input type="checkbox" id="tolak" className="form-check" 
                            checked={RefreshModel.tolak} onChange={checkedHandler("tolak")} /><br></br>
                              <label className="form-check-label" htmlFor="tolak">Tolak</label>
                            <label className='form-label'> Nama Lengkap*</label><br/>
                            <input type="text" id="project_name" className="form-control" 
                            value={RefreshModel.nama} onChange={changeHandler("nama")}/>
                            {/* <span style={{color: "nama"}}>{errors["nama"]}</span><br/> */}
                            <label className='form-label'> Umur*</label><br/>
                            <input type="text" id="umur" className="form-control"  
                            value={RefreshModel.umur} onChange={changeHandler("umur")}/>
                            {/* <span style={{color: "red"}}>{errors["umur"]}</span><br/> */}
                        
                            
                            <label className='form-label'> No Handphone yang bisa dihubungi*</label><br/>
                            <input type="text" id="no_hp" className="form-control" 
                            value={RefreshModel.no_hp} onChange={changeHandler("no_hp")}/><br/>
                            <span style={{color: "red"}}>{errors["no_hp"]}</span><br/>
                            Status*
                            <input type="checkbox" id="dosen" className="form-check" 
                            checked={RefreshModel.dosen} onChange={checkedHandler("dosen")} /><br></br>
                              <label className="form-check-label" htmlFor="dosen">Dosen</label>
                              <input type="checkbox" id="tenaga_kependidikan" className="form-check" 
                            checked={RefreshModel.tenaga_kependidikan} onChange={checkedHandler("tenaga_kependidikan")} /><br></br>
                              <label className="form-check-label" htmlFor="tenaga_kependidikan">Tenaga kependidikan</label>
                              <input type="checkbox" id="cleaning_service" className="form-check" 
                            checked={RefreshModel.cleaning_service} onChange={checkedHandler("cleaning_service")} /><br></br>
                              <label className="form-check-label" htmlFor="cleaning_service">Cleaning service (CS)</label>
                              <input type="checkbox" id="security" className="form-check" 
                            checked={RefreshModel.security} onChange={checkedHandler("security")} /><br></br>
                              <label className="form-check-label" htmlFor="security">Security</label>
                              <input type="checkbox" id="mahasiswa" className="form-check" 
                            checked={RefreshModel.mahasiswa} onChange={checkedHandler("mahasiswa")} /><br></br>
                              <label className="form-check-label" htmlFor="mahasiswa">Mahasiswa</label>
                              <input type="checkbox" id="other_status" className="form-check" 
                            checked={RefreshModel.other_status} onChange={checkedHandler("other_status")} /><br></br>
                              <label className="form-check-label" htmlFor="other_status">Yang Lain</label>
                              <input type="text" id="status_other"  className="form-control" 
                            value={RefreshModel.status_other} onChange={changeHandler("status_other")}/><br/>
                            <label className='form-label'>Alamat tempat tinggal saat ini (Kelurahan)*</label><br/>
                            <input type="text" id="kelurahan"  className="form-control" 
                            value={RefreshModel.kelurahan} onChange={changeHandler("kelurahan")}/><br/>
                            {/* <span style={{color: "red"}}>{errors["kelurahan"]}</span><br/> */}
                            
                            <label className='form-label'> Alamat tempat tinggal saat ini (Kecamatan)*</label><br/>
                            <input type="text" id="kecamatan" className='form-control'  
                            value={RefreshModel.kecamatan} onChange={changeHandler("kecamatan")}/>
                            {/* <span style={{color: "red"}}>{errors["kecamatan"]}</span><br/> */}
                        
                            
                            <label className='form-label'>Alamat tempat tinggal saat ini (Kabupaten)*</label><br/>
                            <input type="text" id="kabupaten" className="form-control" 
                            value={RefreshModel.kabupaten} onChange={changeHandler("kabupaten")}/>
                            {/* <span style={{color: "red"}}>{errors["kabupaten"]}</span> */}
                            Fakultas
                            <input type="checkbox" id="pertanian" className="form-check" 
                            checked={RefreshModel.pertanian} onChange={checkedHandler("pertanian")} /><br></br>
                              <label className="form-check-label" htmlFor="pertanian">Pertanian</label>
                              <input type="checkbox" id="biologi" className="form-check" 
                            checked={RefreshModel.biologi} onChange={checkedHandler("biologi")} /><br></br>
                              <label className="form-check-label" htmlFor="mahasiswa">Biologi</label>
                            <input type="checkbox" id="ekonomi" className="form-check" 
                            checked={RefreshModel.ekonomi} onChange={checkedHandler("ekonomi")} /><br></br>
                              <label className="form-check-label" htmlFor="ekonomi">Ekonomi dan Bisnis</label>
                              <input type="checkbox" id="peternakan" className="form-check" 
                            checked={RefreshModel.peternakan} onChange={checkedHandler("peternakan")} /><br></br>
                              <label className="form-check-label" htmlFor="peternakan">Peternakan</label>
                              <input type="checkbox" id="hukum" className="form-check" 
                            checked={RefreshModel.hukum} onChange={checkedHandler("hukum")} /><br></br>
                              <label className="form-check-label" htmlFor="hukum">Hukum</label>
                              <input type="checkbox" id="ilmu_sosial" className="form-check" 
                            checked={RefreshModel.ilmu_sosial} onChange={checkedHandler("ilmu_sosial")} /><br></br>
                              <label className="form-check-label" htmlFor="ilmu_sosial">Ilmu Sosial dan Politik</label>
                              <input type="checkbox" id="kedokteran" className="form-check" 
                            checked={RefreshModel.kedokteran} onChange={checkedHandler("kedokteran")} /><br></br>
                              <label className="form-check-label" htmlFor="kedokteran">Kedokteran</label>
                              <input type="checkbox" id="teknik" className="form-check" 
                            checked={RefreshModel.teknik} onChange={checkedHandler("teknik")} /><br></br>
                              <label className="form-check-label" htmlFor="teknik">Teknik</label>
                              <input type="checkbox" id="ilmu_kesehatan" className="form-check" 
                            checked={RefreshModel.ilmu_kesehatan} onChange={checkedHandler("ilmu_kesehatan")} /><br></br>
                              <label className="form-check-label" htmlFor="ilmu_kesehatan">Ilmu-ilmu Kesehatan</label>
                              <input type="checkbox" id="mipa" className="form-check" 
                            checked={RefreshModel.mipa} onChange={checkedHandler("mipa")} /><br></br>
                              <label className="form-check-label" htmlFor="mipa">MIPA</label>
                              <input type="checkbox" id="program_pascasarjana" className="form-check" 
                            checked={RefreshModel.program_pascasarjana} onChange={checkedHandler("program_pascasarjana")} /><br></br>
                              <label className="form-check-label" htmlFor="program_pascasarjana">Program Pascasarjana</label>
                              <input type="checkbox" id="ilmu_budaya" className="form-check" 
                            checked={RefreshModel.ilmu_budaya} onChange={checkedHandler("ilmu_budaya")} /><br></br>
                              <label className="form-check-label" htmlFor="ilmu_budaya">Ilmu Budaya</label>
                              <input type="checkbox" id="perikanan" className="form-check" 
                            checked={RefreshModel.perikanan} onChange={checkedHandler("perikanan")} /><br></br>
                              <label className="form-check-label" htmlFor="perikanan">Perikanan dan Ilmu Kelautan</label>
                              <input type="checkbox" id="other_fakultas" className="form-check" 
                            checked={RefreshModel.other_fakultas} onChange={checkedHandler("other_fakultas")} /><br></br>
                              <label className="form-check-label" htmlFor="other_fakultas">Yang Lainnya</label>
                              <input type="text" id="fakultas_other" className="form-control" 
                            value={RefreshModel.fakultas_other} onChange={changeHandler("fakultas_other")}/>

                            <label className='form-label'>Jurusan/Dapartemen/Program/Studi/Subunit Kerja*</label><br/>
                            <input type="text" id="jurusan" className="form-control" 
                            value={RefreshModel.jurusan} onChange={changeHandler("jurusan")}/>
                         

                            Apakah anda sedang mengalami gejala seperti berikut dalam 14 hari terakhir (boleh memilih lebih dari satu)*
                            <input type="checkbox" id="demam" className="form-check" 
                            checked={RefreshModel.demam} onChange={checkedHandler("demam")} /><br></br>
                              <label className="form-check-label" htmlFor="demam">Demam/riwayat demam dalam 14 hari terakhir</label>
                              <input type="checkbox" id="batuk" className="form-check" 
                            checked={RefreshModel.batuk} onChange={checkedHandler("batuk")} /><br></br>
                              <label className="form-check-label" htmlFor="batuk">Batuk/pilek/nyeri tenggorokan</label>
                              <input type="checkbox" id="sesak" className="form-check" 
                            checked={RefreshModel.sesak} onChange={checkedHandler("sesak")} /><br></br>
                              <label className="form-check-label" htmlFor="sesak">Sesak Napas</label>
                              <input type="checkbox" id="tidak_keluhan" className="form-check" 
                            checked={RefreshModel.tidak_keluhan} onChange={checkedHandler("tidak_keluhan")} /><br></br>
                              <label className="form-check-label" htmlFor="tidak_keluhan">Saya tidak mengalami keluhan seperti di atas</label>
                                
                                Apakah anda memiliki faktor resiko melakukan perjalanan keluar negeri atau kota-kota
                                terjangkit di Indonesia dalam waktu 14 hari terakhir (boleh memilih lebih dari satu?*
                                    <input type="checkbox" id="jakarta" className="form-check" 
                            checked={RefreshModel.jakarta} onChange={checkedHandler("jakarta")} /><br></br>
                              <label className="form-check-label" htmlFor="jakarta">Jakarta</label>
                              <input type="checkbox" id="bandung" className="form-check" 
                            checked={RefreshModel.bandung} onChange={checkedHandler("bandung")} /><br></br>
                              <label className="form-check-label" htmlFor="bandung">Bandung</label>
                              <input type="checkbox" id="yogyakarta" className="form-check" 
                            checked={RefreshModel.yogyakarta} onChange={checkedHandler("yogyakarta")} /><br></br>
                              <label className="form-check-label" htmlFor="yogyakarta">Yogyakarta</label>
                              <input type="checkbox" id="depok" className="form-check" 
                            checked={RefreshModel.depok} onChange={checkedHandler("depok")} /><br></br>
                              <label className="form-check-label" htmlFor="depok">Depok</label>
                              <input type="checkbox" id="tangerang" className="form-check" 
                            checked={RefreshModel.tangerang} onChange={checkedHandler("tangerang")} /><br></br>
                              <label className="form-check-label" htmlFor="tangerang">Tangerang</label>
                              <input type="checkbox" id="bogor" className="form-check" 
                            checked={RefreshModel.bogor} onChange={checkedHandler("bogor")} /><br></br>
                              <label className="form-check-label" htmlFor="bogor">Bogor</label>
                              <input type="checkbox" id="manado" className="form-check" 
                            checked={RefreshModel.manado} onChange={checkedHandler("manado")} /><br></br>
                              <label className="form-check-label" htmlFor="manado">Manado</label>
                              <input type="checkbox" id="pontianak" className="form-check" 
                            checked={RefreshModel.pontianak} onChange={checkedHandler("pontianak")} /><br></br>
                              <label className="form-check-label" htmlFor="pontianak">Pontianak</label>
                              <input type="checkbox" id="solo" className="form-check" 
                            checked={RefreshModel.solo} onChange={checkedHandler("solo")} /><br></br>
                              <label className="form-check-label" htmlFor="solo">Solo</label>
                              <input type="checkbox" id="denpasar" className="form-check" 
                            checked={RefreshModel.denpasar} onChange={checkedHandler("denpasar")} /><br></br>
                              <label className="form-check-label" htmlFor="denpasar">Denpasar</label>
                              <input type="checkbox" id="tidak_riwayat" className="form-check" 
                            checked={RefreshModel.tidak_riwayat} onChange={checkedHandler("tidak_riwayat")} /><br></br>
                              <label className="form-check-label" htmlFor="tidak_riwayat">Saya tidak memiliki riwayat perjalanan keluar negeri maupun kota terjangkit</label>
                              <input type="checkbox" id="other_riwayat" className="form-check" 
                            checked={RefreshModel.other_riwayat} onChange={checkedHandler("other_riwayat")} /><br></br>
                              <label className="form-check-label" htmlFor="other_riwayat">Yang Lain</label>
                              <input type="text" id="riwayat_other" className="form-control" 
                            value={RefreshModel.riwayat_other} onChange={changeHandler("riwayat_other")}/>

                              Apakah anda memiliki riwayat paparan risikoberikut dalam 14vhari terakhir? (boleh memilih lebih dari satu)*
                              <input type="checkbox" id="kontak_pasien" className="form-check" 
                            checked={RefreshModel.kontak_pasien} onChange={checkedHandler("kontak_pasien")} /><br></br>
                              <label className="form-check-label" htmlFor="kontak_pasien">Riwayat kontak erat dengan pasien yang terkontimasi atau suspect/terduga terkena COVID-19</label>
                              <input type="checkbox" id="bekerja_kesehatan" className="form-check" 
                            checked={RefreshModel.bekerja_kesehatan} onChange={checkedHandler("bekerja_kesehatan")} /><br></br>
                              <label className="form-check-label" htmlFor="bekerja_kesehatan">Bekerja atau mengunjungi fasilitas kesehatan yang berhubungan dengan pasien yang terkonfirmasi
                              atau suspect/terduga terkenan COVID-19</label>
                              <input type="checkbox" id="memiliki_demam" className="form-check" 
                            checked={RefreshModel.memiliki_demam} onChange={checkedHandler("memiliki_demam")} /><br></br>
                              <label className="form-check-label" htmlFor="memiliki_demam">Memiliki demam (>38 derajat C) atau ada riwayat demam 
                              dalam 14 hari terakhir, memiliki riwayat perjalanan keluar negeri atau kontak dengan orang yang memiliki riwaayat perjalanan keluar negeri</label>
                              <input type="checkbox" id="tidak_resiko" className="form-check" 
                            checked={RefreshModel.tidak_resiko} onChange={checkedHandler("tidak_resiko")} /><br></br>
                              <label className="form-check-label" htmlFor="tidak_resiko">Saya tidak mempunyai faktor resiko seperti di atas</label>

                              Transportasi umum yang bisa digunakan dalam kurun waktu 14 hari terakhir 
                              (boleh memilih lebih dari satu)*
                              <input type="checkbox" id="kendaraan_pribadi" className="form-check" 
                            checked={RefreshModel.kendaraan_pribadi} onChange={checkedHandler("kendaraan_pribadi")} /><br></br>
                              <label className="form-check-label" htmlFor="kendaraan_pribadi">Kendaraan Pribadi</label>
                              <input type="checkbox" id="kereta_api" className="form-check" 
                            checked={RefreshModel.kereta_api} onChange={checkedHandler("kereta_api")} /><br></br>
                              <label className="form-check-label" htmlFor="kereta_api">Kereta Api</label>
                              <input type="checkbox" id="bus" className="form-check" 
                            checked={RefreshModel.bus} onChange={checkedHandler("bus")} /><br></br>
                              <label className="form-check-label" htmlFor="bus">Bus</label>
                              <input type="checkbox" id="angkot" className="form-check" 
                            checked={RefreshModel.angkot} onChange={checkedHandler("angkot")} /><br></br>
                              <label className="form-check-label" htmlFor="angkot">Angkot</label>
                              <input type="checkbox" id="taksi" className="form-check" 
                            checked={RefreshModel.taksi} onChange={checkedHandler("taksi")} /><br></br>
                              <label className="form-check-label" htmlFor="taksi">Taksi</label>
                              <input type="checkbox" id="ojek_online" className="form-check" 
                            checked={RefreshModel.ojek_online} onChange={checkedHandler("ojek_online")} /><br></br>
                              <label className="form-check-label" htmlFor="ojek_online">Ojek Online (mobil atau motor)</label>
                              <input type="checkbox" id="other_tranportasi" className="form-check" 
                            checked={RefreshModel.other_transportasi} onChange={checkedHandler("other_transportasi")} /><br></br>
                              <label className="form-check-label" htmlFor="other_transportasi">Yang Lainnya</label>
                              <input type="text" id="transportasi_other" className="form-control" 
                            value={RefreshModel.transportasi_other} onChange={changeHandler("transportasi_other")}/>

                        </div>   
                    </Modal.Body>
                    <Modal.Footer style={{backgroundColor: "white",width:'150%' }}>
                        <div className="modal-footer">
                        <button style={{backgroundColor:'#ff6600', borderColor:'#ff6600'}} onClick={cancelHandle}><font color="white">Reset</font></button>
                            <button style={{backgroundColor:"#000066", borderColor:"#000066"}} onClick={onSave}><font color="white">Simpan</font></button>
                        </div>
                    </Modal.Footer>
                </Modal>  
            </div>
        )
    }
}   
export default FormInputRefresh;