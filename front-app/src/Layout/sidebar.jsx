import React from 'react';
import { Link } from 'react-router-dom';

export default class sidebar extends React.Component {
    render() {
        return (

            <aside class="main-sidebar">
                <section class="sidebar">
                    
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="active treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Refresh Project</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                           
                                <li><a href="Resource"><i class="fa fa-circle-o"></i>Refresh</a></li>
                                <li><a href="Refresh"><i class="fa fa-circle-o"></i>Refresh Project</a></li>
                               
                               
                            </ul>
                       
                            
                        </li>
                       
                        
                       
                    
                    </ul>
                </section>
            </aside>

        )
    }
}