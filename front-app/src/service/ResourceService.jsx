import axios from 'axios';
import {config} from '../config/config';

const ResourceService = {
    getAllresource: (filter) => {
        const result = axios.post(config.apiUrl + '/getresource', filter)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
    },


    post: (item) => {
        const result = axios.post(config.apiUrl + '/resourcepost', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    updateresource: (item) => {
        const result = axios.put(config.apiUrl + '/updateresource/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getdetailbyid:(id) => {
        const result = axios.get(config.apiUrl+'/getdetailbyid/'+id)
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },

    getdeletebyid:(id) => {
        const result = axios.get(config.apiUrl+'/getdeletebyid/'+id)
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },

    delete: (item) => {
        const result = axios.put(config.apiUrl + '/deleteresource/'+item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;

    },
    
    getdatabyid: (id) => {
        const result = axios.get(config.apiUrl + '/getresourcebyid/'+ id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error

                }
            });
        return result;
    },

    countresource:(filter) => {
        const result = axios.post(config.apiUrl+'/countresource', filter)
        .then(response =>{
            return{
                success: response.data.success, 
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },
    
}
export default ResourceService;

