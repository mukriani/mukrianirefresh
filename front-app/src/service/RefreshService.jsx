import axios from 'axios';
import {config} from '../config/config';

const RefreshService = {
    getAllrefresh: () => {
        const result = axios.get(config.apiUrl + '/getrefresh')
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });
        return result;
        console.log(result)
    },


    post: (item) => {
        const result = axios.post(config.apiUrl + '/refreshpost', item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },
    updateresource: (item) => {
        const result = axios.put(config.apiUrl + '/updaterefresh/' + item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;
    },

    getdetailbyid:(id) => {
        const result = axios.get(config.apiUrl+'/getdetailbyid/'+id)
        .then(response =>{
            return{
                success: response.data.success,
                result: response.data.result
            }
        })
        .catch(error =>{
            return{
                success: false,
                result: error
            }
        });
        return result;
    },

    delete: (item) => {
        const result = axios.put(config.apiUrl + '/deleterefresh/'+item.id, item)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            });

        return result;

    },
    
    getdatabyid: (id) => {
        const result = axios.get(config.apiUrl + '/getrefreshbyid/'+ id)
            .then(respons => {
                return {
                    success: respons.data.success,
                    result: respons.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error

                }
            });
        return result;
    },
    
}
export default RefreshService;

