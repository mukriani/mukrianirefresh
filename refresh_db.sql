PGDMP                 	        x         
   refresh_db    11.7    11.7 	    �
           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �
           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �
           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �
           1262    57490 
   refresh_db    DATABASE     �   CREATE DATABASE refresh_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE refresh_db;
             postgres    false            �            1259    57493    refresh_project    TABLE     �  CREATE TABLE public.refresh_project (
    email character varying(50) NOT NULL,
    bersedia boolean,
    tolak boolean,
    nama character varying(60) NOT NULL,
    umur bigint NOT NULL,
    no_hp bigint NOT NULL,
    dosen boolean,
    tenaga_kependidikan boolean,
    cleaning_service boolean,
    security boolean,
    mahasiswa boolean,
    other_status boolean,
    status_other character varying(40),
    kelurahan character varying(40) NOT NULL,
    kecamatan character varying(40) NOT NULL,
    kabupaten character varying(40) NOT NULL,
    pertanian boolean,
    biologi boolean,
    ekonomi boolean,
    peternakan boolean,
    hukum boolean,
    ilmu_sosial boolean,
    teknik boolean,
    ilmu_kesehatan boolean,
    mipa boolean,
    program_pascasarjana boolean,
    ilmu_budaya boolean,
    perikanan boolean,
    other_fakultas boolean,
    fakultas_other character varying(30),
    jurusan character varying(30),
    demam boolean,
    batuk boolean,
    sesak boolean,
    tidak_keluhan boolean,
    jakarta boolean,
    bandung boolean,
    yogyakarta boolean,
    depok boolean,
    tangerang boolean,
    manado boolean,
    pontianak boolean,
    solo boolean,
    denpasar boolean,
    tidak_riwayat boolean,
    other_riwayat boolean,
    riwayat_other character varying(30),
    kontak_pasien boolean,
    bekerja_kesehatan boolean,
    memiliki_demam boolean,
    tidak_resiko boolean,
    kendaraan_pribadi boolean,
    kereta_api boolean,
    bus boolean,
    angkot boolean,
    taksi boolean,
    ojek_online boolean,
    other_kendaraan boolean,
    kendaraan_other character varying(30),
    id bigint NOT NULL,
    kedokteran boolean,
    bogor boolean
);
 #   DROP TABLE public.refresh_project;
       public         postgres    false            �            1259    57491    refresh_project_id_seq    SEQUENCE     �   ALTER TABLE public.refresh_project ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.refresh_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999
    CACHE 1
);
            public       postgres    false    197            �
          0    57493    refresh_project 
   TABLE DATA               �  COPY public.refresh_project (email, bersedia, tolak, nama, umur, no_hp, dosen, tenaga_kependidikan, cleaning_service, security, mahasiswa, other_status, status_other, kelurahan, kecamatan, kabupaten, pertanian, biologi, ekonomi, peternakan, hukum, ilmu_sosial, teknik, ilmu_kesehatan, mipa, program_pascasarjana, ilmu_budaya, perikanan, other_fakultas, fakultas_other, jurusan, demam, batuk, sesak, tidak_keluhan, jakarta, bandung, yogyakarta, depok, tangerang, manado, pontianak, solo, denpasar, tidak_riwayat, other_riwayat, riwayat_other, kontak_pasien, bekerja_kesehatan, memiliki_demam, tidak_resiko, kendaraan_pribadi, kereta_api, bus, angkot, taksi, ojek_online, other_kendaraan, kendaraan_other, id, kedokteran, bogor) FROM stdin;
    public       postgres    false    197   �       �
           0    0    refresh_project_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.refresh_project_id_seq', 1, true);
            public       postgres    false    196            }
           2606    57497 $   refresh_project refresh_project_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.refresh_project
    ADD CONSTRAINT refresh_project_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.refresh_project DROP CONSTRAINT refresh_project_pkey;
       public         postgres    false    197            �
   h   x��-�.�4432wH�M���K�����\�Lb^&��1�����������Te�&%V�%�!����J�k��JR��2�RsR�K��I�H<2��\1z\\\ P`B�     